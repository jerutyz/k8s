for i in `cat hosts`;do
	ssh -t $i "sudo apt -y install apt-transport-https ca-certificates curl software-properties-common"
	ssh -t $i "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -"
	ssh -t $i "sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable' && sudo apt-get update"
	ssh -t $i "sudo apt-cache policy docker-ce"
	ssh -t $i "sudo apt-get install -y docker-ce"
	ssh -t $i "sudo systemctl status docker"
	ssh -t $i "sudo usermod -aG docker user"
done
